<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Team;
use App\Rider;

class Rider extends Model
{

	public function teams(){
		return $this->hasMany(Team::class,'teams','team_id')->withTimestamps();
	
	}
}
