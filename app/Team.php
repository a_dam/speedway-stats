<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Rider;
use App\Team;

class Team extends Model
{
    protected $fillable = ['name','city'];

    public function riders(){
    	return $this->hasMany(App\Rider);
    }
}
