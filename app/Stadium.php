<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stadium extends Model
{
	protected $fillable = ['name', 'city', 'length_of_track', 'record_of_track', 'track_record_holder'];
}
