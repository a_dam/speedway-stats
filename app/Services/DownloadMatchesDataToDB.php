<?php

    namespace App\Services;

    use App\Services\MatchesParser;
    use App\Services\RidersParser;
    use App\Match;
    use App\Team;
    use App\Stadium;
    use App\Gate;
    use App\RidersResults;
    use App\Rider;
    use App\Heat;
    use App\Result;
    use DB;

    class DownloadMatchesDataToDB{

      public function getGateID($gate){
        $gate = Gate::where('gate',$gate)->first();
        return $gate->id;
      }

      public function getTeamID($team){
        $team = Team::where(DB::raw('concat(name," ",city)') , 'LIKE' , $team)->first();
        return $team;
      }

      public function getMatchID($tag){
        $match = Match::where('tag',$tag)->first();
        return $match->id;
      }

      public function getResultID($result){
        $result = Result::where('result',$result)->first();
        return $result->id;
      }

      public function getRiderID($rider){
        $ridersParser = new RidersParser();
        $eastSlavicNamesExceptions = $ridersParser->getEastSlavicSuramesExceptions();
        for($j=0;$j<count($eastSlavicNamesExceptions);$j++){
            if(trim($eastSlavicNamesExceptions[$j]['surname'])==trim($rider)){
              $rider = Rider::where(DB::raw('concat(name," ",surname)') , 'LIKE' , $eastSlavicNamesExceptions[$j]['key'])->first();
              return $rider->id;
            }
        }
        $rider = Rider::where(DB::raw('concat(name," ",surname)') , 'LIKE' ,trim($rider))->first();
        return $rider->id;
      }

      public function getHeatID($number,$tag){
        $heat = Heat::where('match_id',$this->getMatchID($tag))->where('number',$number)->first();
        return $heat->id;
      }

      public function checkBonusPoint($heats,$heat){
        for ($i=0; $i < count($heats); $i++) {
          if($heats[$i]['heat']==$heat){
            if(strcmp('0',$heats[$i]['riderResult'])!=-1){
              return "*";
            }
          }
        }
      }

      public function getPoint($result,$heats,$heatsInfo,$heat,$gate){
        if(trim($heatsInfo)=="3 : 3"){
          if($result==1)
            return $this->checkBonusPoint($heats,$heat);

        }
        elseif (trim($heatsInfo)=="5 : 1" || trim($heatsInfo)=="1 : 5") {
          if($result==2)
          return $this->checkBonusPoint($heats,$heat);
        }
      }

      public function getFalstartRider($rider){
        return substr($rider,7);
      }

      public function saveMatchInfo($matchInfo){
        $match = new Match();
        $match->tag = $matchInfo[0]['gameDay']."_".$matchInfo[0]['matchTag'];
        $match->stadium_id = Stadium::where('city','like', $this->getTeamID($matchInfo[0]['homeTeam'])->city)->first()->id;
        $match->date = $matchInfo[0]['date'];
        $match->time = $matchInfo[0]['time'];
        $match->game_day =  $matchInfo[0]['gameDay'];
        $match->home_team = $this->getTeamID($matchInfo[0]['homeTeam'])->id;
        $match->away_team = $this->getTeamID($matchInfo[0]['awayTeam'])->id;
        $match->result = $matchInfo[0]['matchResult'];
        $match->save();
      }

      public function saveHeatsInfo($heatsInfo,$tag){
        for ($i=0; $i < count($heatsInfo) ; $i++) {
          $heat = new Heat();
          $heat->match_id = $this->getMatchID($tag);
          $heat->number = $heatsInfo[$i]['number'];
          $heat->time = $heatsInfo[$i]['time'];
          $heat->result = $heatsInfo[$i]['result'];
          $heat->save();
        }
      }

      public function saveHeats($heats,$heatsInfo,$tag){
        for ($i=0; $i < count($heats) ; $i++) {
          $riderResult = new RidersResults();
          $riderResult->gate_id = $this->getGateID($heats[$i]['gate']);
          $riderResult->rider_id = $this->getRiderID($heats[$i]['rider']);
          $riderResult->result_id = $this->getResultID(trim($heats[$i]['riderResult']) . $this->getPoint($heats[$i]['riderResult'],$heats,$heatsInfo[($heats[$i]['heat'])-1]['result'],$heats[$i]['heat'],$heats[$i]['gate']));
          $riderResult->heat_id = $this->getHeatID($heats[$i]['heat'],$tag);
          $riderResult->match_id = $this->getMatchID($tag);
          $riderResult->save();
        }
        for ($j=0;$j<count($heats); $j++) {
          if(strpos($heats[$j]['reserve'],"Taśma")===0){
            $riderResult = new RidersResults();
            $riderResult->gate_id = $this->getGateID($heats[$j]['gate']);
            $riderResult->rider_id = $this->getRiderID($this->getFalstartRider($heats[$j]['reserve']));
            $riderResult->result_id = $this->getResultID(trim($heats[$j]['reserveResult']));
            $riderResult->heat_id = $this->getHeatID($heats[$i]['heat'],$tag);
            $riderResult->match_id = $this->getMatchID($tag);
            $riderResult->save();
          }
        }
      }

      public function downloadMatchData($link){
        $parser = new MatchesParser();
        $linkFromForm = "http://sportowefakty.wp.pl/zuzel/relacja/$link";
        $url =  file_get_contents( $linkFromForm );
        $matchInfo = $parser -> getMatchInfo($url);
        $heatsInfo = $parser -> getHeatsInfo($url);
        $heats = $parser -> getHeats($url);
        $this->saveMatchInfo($matchInfo);
        $this->saveHeatsInfo($heatsInfo,$matchInfo[0]['gameDay']."_".$matchInfo[0]['matchTag']);
        $this->saveHeats($heats,$heatsInfo,$matchInfo[0]['gameDay']."_".$matchInfo[0]['matchTag']);
      }
    }
