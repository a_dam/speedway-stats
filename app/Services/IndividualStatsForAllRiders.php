<?php

  namespace App\Services;
  use DB;
  use App\Result;

  class IndividualStatsForAllRiders{

    public function getRiders(){
      $riders = DB::table('riders')
  		->join('teams', 'teams.id', '=', 'riders.teams_id')
  		->select('riders.*', 'teams.name as team' ,'teams.id as team_id')
  		->get();
      return $riders;
    }

    public function getFirstPlaces($riders){
      for($i=0;$i<count($riders);$i++){
        $firstPlaces[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
  								->where('result_id','=',Result::where('result',3)->first()->id)
                  ->count();
      }
      return $firstPlaces;
    }

    public function getSecondPlaces($riders){
      for($i=0;$i<count($riders);$i++){
        $secondPlaces[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result',2)->first()->id)
                  ->count();
        $bonusPoints[$i] =  DB::table('riders_results')
                              ->where('rider_id','=',$riders[$i]->id)
                              ->where('result_id','=',Result::where('result','2*')->first()->id)
                              ->count();
        $secondPlaces[$i]=$secondPlaces[$i]+$bonusPoints[$i];
      }
      return $secondPlaces;
    }

    public function getThirdPlaces($riders){
      for($i=0;$i<count($riders);$i++){
        $thirdPlaces[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result',1)->first()->id)
                  ->count();
        $bonusPoints[$i] =  DB::table('riders_results')
                              ->where('rider_id','=',$riders[$i]->id)
                              ->where('result_id','=',Result::where('result','1*')->first()->id)
                              ->count();
        $thirdPlaces[$i]=$thirdPlaces[$i]+$bonusPoints[$i];
      }
      return $thirdPlaces;
    }

    public function getFourthPlaces($riders){
      for($i=0;$i<count($riders);$i++){
        $fourthPlaces[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
  								->where('result_id','=',Result::where('result',0)->first()->id)
                  ->count();
      }
      return $fourthPlaces;
    }

    public function getExclusions($riders){
      for($i=0;$i<count($riders);$i++){
        $exclusions[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result','w')->first()->id)
                  ->count();
      }
      return $exclusions;
    }

    public function getDefects($riders){
      for($i=0;$i<count($riders);$i++){
        $defects[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result','d')->first()->id)
                  ->count();
      }
      return $defects;
    }

    public function getFalstarts($riders){
      for($i=0;$i<count($riders);$i++){
        $falstarts[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result','t')->first()->id)
                  ->count();
      }
      return $falstarts;
    }

    public function getCrashes($riders){
      for($i=0;$i<count($riders);$i++){
        $crashes[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result','u')->first()->id)
                  ->count();
      }
      return $crashes;
    }

    public function getPoints($riders,$firstPlaces,$secondPlaces,$thirdPlaces){
      for($i=0;$i<count($riders);$i++){
        $points[$i] = $firstPlaces[$i]*3 + $secondPlaces[$i]*2 + $thirdPlaces[$i];
      }
      return $points;
    }

    public function getBonusPoints($riders){
      for($i=0;$i<count($riders);$i++){
        $bonusPointsSecondPlace[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result','2*')->first()->id)
                  ->count();
        $bonusPointsThirdPlace[$i] = DB::table('riders_results')
                  ->where('rider_id','=',$riders[$i]->id)
                  ->where('result_id','=',Result::where('result','1*')->first()->id)
                  ->count();
        $bonusPoints[$i] = $bonusPointsSecondPlace[$i] + $bonusPointsThirdPlace[$i];
      }
      return $bonusPoints;
    }

    public function getPointsWithBonusPoints($riders,$points,$bonusPoints){
      for($i=0;$i<count($riders);$i++){
        $pointsWithBonusPoints[$i] = $points[$i] + $bonusPoints[$i];
      }
      return $pointsWithBonusPoints;
    }

    public function getHeats($riders){
      for($i=0;$i<count($riders);$i++){
        $heats[$i] =  DB::table('riders_results')
    									->where('rider_id','=',$riders[$i]->id)
    									->count();
      }
      return $heats;
    }

    public function getHeatAverages($riders,$heats,$pointsWithBonusPoints){
      for($i=0;$i<count($riders);$i++){
        if($heats[$i]!=0)
          $heatAverages[$i] = round($pointsWithBonusPoints[$i] / $heats[$i],3);
        else
        $heatAverages[$i] = 0;
      }
      return $heatAverages;
    }

    public function getMatches($riders){
      for($i=0;$i<count($riders);$i++){
        $matches[$i] =  DB::table('riders_results')
    									->distinct()
    									->select('match_id')
    									->where('rider_id','=',$riders[$i]->id)
    									->get();
      }
      for($i=0;$i<count($riders);$i++){
        $matches[$i]=count($matches[$i]);
      }

      return $matches;
    }

    public function getMatchAverages($riders,$matches,$points){
      for($i=0;$i<count($riders);$i++){
        if($matches[$i]!=0)
          $matchAverages[$i] = $points[$i] / $matches[$i];
        else
        $matchAverages[$i] = 0;
      }
      return $matchAverages;
    }

  }
