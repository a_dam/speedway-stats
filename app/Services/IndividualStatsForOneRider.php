<?php

  namespace App\Services;
  use App\Result;
  use DB;

  class IndividualStatsForOneRider{

    public function getRider($rider){
      $riders = DB::table('riders')
      ->join('teams', 'teams.id', '=', 'riders.teams_id')
      ->select('riders.*', 'teams.name as team')
      ->where('riders.id',$rider)
      ->get();
      return $riders;
    }

    public function getFirstPlaces($rider){
        $firstPlaces = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
  								->where('result_id','=',Result::where('result',3)->first()->id)
                  ->count();

      return $firstPlaces;
    }

    public function getSecondPlaces($rider){
        $secondPlaces = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result',2)->first()->id)
                  ->count();
        $bonusPoints =  DB::table('riders_results')
                              ->where('rider_id','=',$rider)
                              ->where('result_id','=',Result::where('result','2*')->first()->id)
                              ->count();
        $secondPlaces=$secondPlaces+$bonusPoints;
      return $secondPlaces;
    }

    public function getThirdPlaces($rider){
        $thirdPlaces = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result',1)->first()->id)
                  ->count();
        $bonusPoints =  DB::table('riders_results')
                              ->where('rider_id','=',$rider)
                              ->where('result_id','=',Result::where('result','1*')->first()->id)
                              ->count();
        $thirdPlaces=$thirdPlaces+$bonusPoints;
      return $thirdPlaces;
    }

    public function getFourthPlaces($rider){
        $fourthPlaces = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
  								->where('result_id','=',Result::where('result',0)->first()->id)
                  ->count();
      return $fourthPlaces;
    }

    public function getExclusions($rider){
        $exclusions = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result','w')->first()->id)
                  ->count();
      return $exclusions;
    }

    public function getDefects($rider){
        $defects = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result','d')->first()->id)
                  ->count();
      return $defects;
    }

    public function getFalstarts($rider){
        $falstarts = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result','t')->first()->id)
                  ->count();
      return $falstarts;
    }

    public function getCrashes($rider){
        $crashes = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result','u')->first()->id)
                  ->count();
      return $crashes;
    }

    public function getPoints($rider,$firstPlaces,$secondPlaces,$thirdPlaces){
        $points = $firstPlaces*3 + $secondPlaces*2 + $thirdPlaces;
      return $points;
    }

    public function getBonusPoints($rider){
        $bonusPointsSecondPlace = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result','2*')->first()->id)
                  ->count();
        $bonusPointsThirdPlace = DB::table('riders_results')
                  ->where('rider_id','=',$rider)
                  ->where('result_id','=',Result::where('result','1*')->first()->id)
                  ->count();
        $bonusPoints = $bonusPointsSecondPlace + $bonusPointsThirdPlace;
      return $bonusPoints;
    }

    public function getPointsWithBonusPoints($rider,$points,$bonusPoints){
        $pointsWithBonusPoints = $points + $bonusPoints;
      return $pointsWithBonusPoints;
    }

    public function getHeats($rider){
        $heats =  DB::table('riders_results')
    									->where('rider_id','=',$rider)
    									->count();
      return $heats;
    }

    public function getHeatAverages($rider,$heats,$pointsWithBonusPoints){
        if($heats!=0)
          $heatAverages = round($pointsWithBonusPoints / $heats,3);
        else
        $heatAverages = 0;
      return $heatAverages;
    }

    public function getMatches($rider){
      $matches =  DB::table('riders_results')
    									->distinct()
    									->select('match_id')
    									->where('rider_id','=',$rider)
    									->get();
      $matches=count($matches);
      return $matches;
    }

    public function getMatchAverages($rider,$matches,$points){
        if($matches!=0)
          $matchAverages = $points / $matches;
        else
        $matchAverages = 0;
      return $matchAverages;
    }
  }
