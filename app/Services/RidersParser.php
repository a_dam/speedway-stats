<?php

	namespace App\Services;

	class RidersParser{

		public function getRidersExceptions(){
			$exceptions = array(  array('key'  => 'Niels Kristian Iversen',
													 				'name' => 'Niels Kristian',
													 				'surname' => 'Iversen'),
										 			  array('key'  => 'Michael Jepsen Jensen',
													 				'name' => 'Michael',
													 				'surname' => "Jepsen Jensen"));
			return $exceptions;
		}

		public function getEastSlavicSuramesExceptions(){
			$eastSlavicNamesExceptions = array( array('key'  => 'Andriy Karpov',
													       								'surname' => 'Andriej Karpow'),
																				  array('key'  => 'Emil Sayfutdinov',
																							  'surname' => 'Emil Sajfutdinow'),
														 					  	array('key'  => 'Andzej Lebedevs',
																							  'surname' => 'Andrzej Lebiediew'),
																		      array('key'  => 'Grigorij Laguta',
																				  	    'surname' => 'Grigorij Łaguta'),
								       									  array('key'  => 'Artem Laguta',
												       								  'surname' => 'Artiom Łaguta'),
																			  	array('key'  => 'Viktor Trofymov',
																							  'surname' => 'Wiktor Trofimow'),
																					array('key'  => 'Damian Dróżdż',
																							  'surname' => 'Damian Dr&oacute;żdż'));
			return $eastSlavicNamesExceptions;
		}

		public function getRidersNames($url, $exceptions){
			preg_match_all('#<h1 class="player__name">(.*?)</h1>#is', $url, $riders );
			$i = 0;
			foreach ($riders[1] as $rider) {
				for($exception = 0; $exception < 2; $exception++){
							if(strcmp($rider,$exceptions[$exception]['key'])==0){
								$names[$i] = $exceptions[$exception]['name'];
								break;
							}
							else{
								$name = explode(" ",$rider);
								$names[$i] = $name[0];
							}
				}
				$i++;
			}
			return $names;
		}

		public function getRidersSurnames($url,$exceptions){
			preg_match_all('#<h1 class="player__name">(.*?)</h1>#is', $url, $riders );
			$i = 0;
			foreach ($riders[1] as $rider){
				for($exception = 0; $exception < 2; $exception++){
						if(strcmp($rider,$exceptions[$exception]['key'])==0){
							$surnames[$i] = $exceptions[$exception]['surname'];
							break;
						}
						else{
							$surname = explode(" ",$rider);
							$surnames[$i] = $surname[1];
						}
				}
				$i++;
			}
			return $surnames;
		}

		public function getRidersDatesofBirth($url){
			preg_match_all('#<div class="player__year">(.*?)</div>#is', $url, $riders );
			$i = 0;
			foreach ($riders[1] as $rider) {
				$datesOfBirth[$i] = $rider;
				$i++;
			}
			return $datesOfBirth;
		}

		public function getRidersNationality($url){
			preg_match_all('#<div class="player__nationality">(.*?)</div>#is', $url, $riders );
			$i = 0;
			foreach ($riders[1] as $rider) {
				$nationalities[$i] = substr($rider,27);
				$i++;
			}
			return $nationalities;
		}

	}
