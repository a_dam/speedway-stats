<?php

  namespace App\Services;

  use DB;
  use App\Match;
  use App\RidersResults;
  use App\Result;
  use App\Team;

  class MatchResultForRider{

    public function getMatches($rider){
      $matches =  RidersResults::distinct()
                      ->select('match_id')
                      ->where('rider_id','=',$rider)
                      ->get();
      return $matches;
    }

    public function getHeatsInMatch($match,$rider){
      $heats = RidersResults::where('match_id','=',$match)->where('rider_id',$rider)->get();
      $matchInfo = Match::where('id',$match)->first();
      $homeTeam = Team::where('id',$matchInfo->home_team)->first()->name;
      $awayTeam = Team::where('id',$matchInfo->away_team)->first()->name;
      $pointsSum=0;
      $points = "(";
      for($i=0;$i<count($heats);$i++){
        $result = Result::where('id',$heats[$i]->result_id)->first()->result;
        if($result==3 || $result==2 || $result==1)
          $pointsSum += $result;
        $points .= $result;
        if($i<count($heats)-1)
          $points .= ",";
      }
      $points .= ") ";
      return $matchInfo->date ." " .  $matchInfo->time." " . $homeTeam ." " . $awayTeam ." " .$points ." " . $pointsSum ." " . $matchInfo->result;
    }

    public function getResults($rider){
      $matches = $this->getMatches($rider);
      if(count($matches)>0){
        for($i=0;$i<count($matches);$i++){
          $m[$i] = $this->getHeatsInMatch($matches[$i]->match_id,$rider);
        }
        return $m;
      }
      else{
        // for($i=0;$i<count($matches);$i++){
        //   $j[$i] = " ";
        // }
        return "No results";
      }
    }

  }
