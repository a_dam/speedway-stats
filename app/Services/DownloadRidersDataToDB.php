<?php

  namespace App\Services;

  use App\Rider;
  use App\Team;

  class DownloadRidersDataToDB{


    public function downloadRidersData(){

      $parser = new RidersParser();
      $urls[0] = 'https://speedwayekstraliga.pl/druzyny/falubaz-zielona-gora/';
      $urls[1] = 'https://speedwayekstraliga.pl/druzyny/sparta-wroclaw/';
      $urls[2] = 'https://speedwayekstraliga.pl/druzyny/ks-torun/';
      $urls[3] = 'https://speedwayekstraliga.pl/druzyny/row-rybnik/';
      $urls[4] = 'https://speedwayekstraliga.pl/druzyny/fogo-unia-leszno/';
      $urls[5] = 'https://speedwayekstraliga.pl/druzyny/gkm-grudziadz/';
      $urls[6] = 'https://speedwayekstraliga.pl/druzyny/stal-gorzow/';
      $urls[7] = 'https://speedwayekstraliga.pl/druzyny/wlokniarz-czestochowa/';

      for ($i=0; $i < count($urls); $i++){
        $url =  file_get_contents( $urls[$i] );
        $names = $parser-> getRidersNames($url,$parser-> getRidersExceptions());
        $surnames = $parser-> getRidersSurnames($url,$parser-> getRidersExceptions());
        $datesOfBirth = $parser-> getRidersDatesofBirth($url);
        $nationalities = $parser-> getRidersNationality($url);
        for ($j=0;$j<count($names);$j++){
          $date = explode(".", $datesOfBirth[$j]);
          $date =  $date[2] . "-" . $date[1] . "-" . $date[0];
          $rider = new Rider();
          $rider->name = $names[$j];
          $rider->surname = $surnames[$j];
          $rider->nationality = $nationalities[$j];
          $rider->date_of_birth = $date;
          $parseTeam = explode("/", $urls[$i]);
          switch ($parseTeam[4]){
            case "falubaz-zielona-gora":
              $team = Team::where('name','ekantor.pl Falubaz')->first();
              break;
            case "sparta-wroclaw":
              $team = Team::where('name','Betard Sparta')->first();
              break;
            case "ks-torun":
              $team = Team::where('name','Get Well')->first();
              break;
            case "row-rybnik":
              $team = Team::where('name','ROW')->first();
              break;
            case "fogo-unia-leszno":
              $team = Team::where('name','FOGO Unia')->first();
              break;
            case "gkm-grudziadz":
              $team = Team::where('name','MrGarden GKM')->first();
              break;
            case "stal-gorzow":
              $team = Team::where('name','Cash Broker Stal')->first();
              break;
            case "wlokniarz-czestochowa":
              $team = Team::where('name',"W\u{0142}\u{00F3}kniarz Vitroszlif Crossfit")->first();
              break;
          }
          $rider->teams_id = $team->id;
          $rider->save();
        }
      }
    }
  }
