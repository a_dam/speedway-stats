<?php

  namespace App\Services;

  use DB;
  use App\Match;
  use App\RidersResults;
  use App\Result;

  class GatesStats{

    public function getMatches($stadium){
      $matches = Match::where('stadium_id',$stadium)->get();
      return $matches;
    }

    public function getGateHeats($matches,$gate){
        for($i=0;$i<count($matches);$i++){
          $heats = DB::table('riders_results')
          ->select('result_id')
          ->where('match_id',$matches[$i]->id)
          ->where('gate_id',$gate)
          ->where('result_id','<>',7)
      		->get();
        }
        return $heats;
    }

    public function showHeats($heats){
      for ($i=0; $i <count($heats) ; $i++) {
        $result = Result::select('result')->where('id',$heats[$i]->result_id)->first();
        $results[$i] = $result->result;
      }
      return $results;
    }

    public function getPoints($results){
      $winHeat=0;
      for ($i=0; $i < count($results) ; $i++) {
        if($results[$i]==3){
          $winHeat++;
        }
      }
      return round(($winHeat/count($results))*100,2);
    }

    public function getGatesStats($stadium,$gate){
        $matches = $this->getMatches($stadium);
        if(count($matches)>0){
      		$heats = $this->getGateHeats($matches,$gate);
      		$results= $this->showHeats($heats);
      		return $this->getPoints($results) . " %";
        }
        else {
          return "";
        }
    }

  }
