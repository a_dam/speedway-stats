<?php

  namespace App\Services;

  class MatchesParser{

    public function getMonth($monthText){
      $months = array( array('key'  => 'Kwietnia','month' => '04'),
                           array('key'  => 'Maja','month' => '05'),
                           array('key'  => 'Czerwca','month' => '06'),
                           array('key'  => 'Lipca','month' => '07'),
                           array('key'  => 'Sierpnia','month' => '08'),
                           array('key'  => 'Września','month' => '09'),
                           array('key'  => 'Października','month' => '10'));
      for($i=0;$i<count($months);$i++){
        if(strcmp($monthText,$months[$i]['key'])==0){
          return $months[$i]['month'];
        }
      }
    }

    public function getTeamTag($team){
      if(strpos($team,"Falubaz"))
        $tag = "ZIE";
      elseif (strpos($team,"Stal"))
        $tag = "GOR";
      elseif (strpos($team,"Sparta"))
        $tag = "WRO";
      elseif (strpos($team,"Unia Leszno"))
        $tag = "LES";
      elseif (strpos($team,"Toruń"))
        $tag = "TOR";
      elseif (strpos($team,"Częstochowa"))
        $tag = "CZE";
      elseif (strpos($team,"ROW"))
        $tag = "RYB";
      else
        $tag = "GRU";
      return $tag;
    }

  	public function getMatchInfo($url){
      preg_match('#<li><i class="icon-calendar"></i>+\s+<span>(.*?)</span>#is', $url, $dateTime );
      $parser = new MatchesParser();
      $dateTime = explode(" ", $dateTime[1]);
      $date = substr($dateTime[2],0,4) .'-'. $parser->getMonth($dateTime[1]) .'-'. $dateTime[0];
      $time = $dateTime[3];
      preg_match('#<li><i class="icon-map-marker"></i>+\s+<span><a[^>]*>(.*?)</a>#is', $url, $stadium );
      $stadium = $stadium[1];
      preg_match('#<meta name="keywords" content="(.*?)"#is', $url, $teams );
      $teams = explode(",",$teams[1]);
      $homeTeam = $teams[0];
      $awayTeam = $teams[1];
      $matchTag = $this->getTeamTag($homeTeam).$this->getTeamTag($awayTeam);
      preg_match('#<strong class="matchcoverage__result">(.*?)<span#is', $url, $homeTeamResult );
      preg_match('#<strong class="matchcoverage__result">..<span class="matchcolon">:</span>(.*?)</strong>#is', $url, $awayTeamResult );
      $matchResult =  $homeTeamResult[1] . ":" .  $awayTeamResult[1];
      preg_match('#<td>Grupa</td>+\s+[ ]{0,}<td>+\s+[ ]{0,}(.*?)</td>#is', $url, $gameDay );
      $matchInfo = array( array('date'  => $date,
                                'time' => $time,
                                'stadium' => $stadium,
                                'homeTeam' => $homeTeam,
                                'awayTeam' => $awayTeam,
                                'matchTag' => $matchTag,
                                'matchResult' => $matchResult,
                                'gameDay' => trim($gameDay[1])));
      return $matchInfo;
    }

    public function getHeats($url){
      preg_match_all('#<span class="competitor__name">+\s+[ ]{0,}(.*?)<small>#is', $url, $riders );
      preg_match_all('#<span class="competitor__name">[^\>]*>\s*(.*?)</small>#is', $url, $reserves );
      preg_match_all('#<span class="competitor__score">+\s+[ ]{0,}(.*?)</span>#is', $url, $ridersResults );
      $j = 0;
      $heat = 1;
      for($i = count($riders[1])-1; $i>=0;$i--){
        $rider = $riders[1][$i];
        $reserve = $reserves[1][$i];
        $riderResult = $ridersResults[1][$i];

        if(strpos($reserve,"Taśma")===0)
          $reservesResults = "t";
        else  $reservesResults = "";

        if($i%4==1)
          $gate = "B";
        if($i%4==2)
          $gate = "C";
        if($i%4==3)
          $gate = "D";
        if($i%4==0)
          $gate = "A";

        $heats[$j] = array( 'gate' => $gate,
                                  'rider' =>   $rider,
                                  'reserve' =>   $reserve,
                                  'riderResult' =>   $riderResult,
                                  'reserveResult' => $reservesResults,
                                  'heat' => $heat);
       if($i%4==0)
          $heat++;
       $j++;
      }
      return $heats;
    }

    public function getHeatsInfo($url){
      preg_match_all('#<span class="coventry__time">(.*?)</span>#is', $url, $heatsTimes );
      preg_match_all('#<div class="coventry__state">(.*?)</div>#is', $url, $heatsResults );
      $j = 0;
      for($i = count($heatsTimes[1])-1; $i>=0;$i--){
        $heatTime = explode(" ",$heatsTimes[1][($i)]);
        $heatTimes[$i] = $heatTime[0];
        $heatResult[$i] = $heatsResults[1][($i)];
        $heatsInfo[$j] = array( 'number' =>  $j+1,
                                'time' =>   $heatTimes[$i],
                                'result' => $heatResult[$i]);
        $j++;
      }
      return $heatsInfo;
    }

  }
