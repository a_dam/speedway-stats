<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stadium;
use App\Gate;
use App\Services\GatesStats;
use DB;

class StadiumsController extends Controller
{
	public function __construct(){
		$this->middleware('auth', ['except' => [
						'index','stadium'
				]]);
	}

	public function index(){
		$stadiums = Stadium::all();
		return view('stadiums.index', compact('stadiums'));
	}

	public function  add(){
		return view('stadiums.add');
	}
	public function edit(){
		$stadiums = Stadium::all();
		return view('stadiums.edit', compact('stadiums'));
	}

	public function save(Request $request){
		Stadium::create($request->all());
		return redirect('/admin/stadiums/edit');
	}

	public function delete(Request $request){
		DB::table('stadia')->where('id', $request->id)->delete();
		return redirect('/admin/stadiums/edit');
	}

	public function showUpdateForm(Request $request){
		$stadium = Stadium::where('id',$request->id)->first();
		return view('stadiums.update',compact('stadium'));
	}

	public function stadium($stadium){
		$stadiums = Stadium::where('id',$stadium)->get();
		$stats = new GatesStats();
		$gateA = $stats->getGatesStats($stadium,Gate::select('id')->where('gate','A')->first()->id);
		$gateB = $stats->getGatesStats($stadium,Gate::select('id')->where('gate','B')->first()->id);
		$gateC = $stats->getGatesStats($stadium,Gate::select('id')->where('gate','C')->first()->id);
		$gateD = $stats->getGatesStats($stadium,Gate::select('id')->where('gate','D')->first()->id);
		return view('stadiums.stadium',compact('stadiums','gateA','gateB','gateC','gateD'));
	}

	public function update(Request $request){
		DB::table('stadia')
					->where('id', $request->id)
					->update(['name' => $request->name,'city' => $request->city,'length_of_track' => $request->length_of_track,'record_of_track' => $request->record_of_track,'track_record_holder' => $request->track_record_holder]);
		return redirect('/admin/stadiums/edit');
	}
}
