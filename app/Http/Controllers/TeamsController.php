<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Rider;
use App\Match;
use DB;

class TeamsController extends Controller
{

	public function __construct(){
		$this->middleware('auth', ['except' => [
            'index', 'team'
        ]]);
	}

	public function index(){
		$teams = Team::all();
		return view('teams.index', compact('teams'));
	}

	public function team($team){
		$teams = Team::where('id',$team)->get();
	  $riders = Rider::where('teams_id',$team)->get();
		$m = DB::table('matches')
		->join('teams', 'matches.home_team', '=', 'teams.id')
		->select('matches.*', 'teams.name as team')
		->where('away_team',$team);
		// ->orWhere('home_team',$team);
		// ->get();

		// echo $matches;
		$matches = DB::table('matches')
		->join('teams', 'matches.away_team', '=', 'teams.id')
		->select('matches.*', 'teams.name as team')
		->where('home_team',$team)
		->union($m)
		->get();

		return view('teams.team', compact('teams','riders','matches'));
	}

  public function add(){
  	return view('teams.add');
  }

	public function edit(){
		$teams = Team::all();
		return view('teams.edit', compact('teams'));
	}

  public function save(Request $request){
		Team::create($request->all());
		return redirect('/admin/teams/edit');
  }

	public function delete(Request $request){
		DB::table('teams')->where('id', $request->id)->delete();
		return redirect('/admin/teams/edit');
	}

	public function showUpdateForm(Request $request){
		$team = Team::where('id',$request->id)->first();
		return view('teams.update',compact('team'));
	}

	public function update(Request $request){
		DB::table('teams')
          ->where('id', $request->id)
          ->update(['name' => $request->name,'city' => $request->city]);
		return redirect('/admin/teams/edit');
	}
}
