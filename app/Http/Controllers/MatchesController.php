<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DownloadMatchesDataToDB;
use DB;
use App\Team;
use App\Stadium;
use App\Match;
use App\Heat;
use App\RidersResults;
use App\Rider;
use App\Gate;
use App\Result;

class MatchesController extends Controller
{
  public function __construct(){
    $this->middleware('auth', ['except' => [
            'index','match'
        ]]);
  }

  public function index(){
    $teams = Team::all();
    $stadiums = Stadium::all();
    $m = DB::table('matches')
		->join('teams', 'matches.home_team', '=', 'teams.id')
		->select('matches.*', 'teams.name as team');
		$matches = DB::table('matches')
		->join('teams', 'matches.away_team', '=', 'teams.id')
		->select('matches.*', 'teams.name as team')
		->get();


    return view('matches.index', compact('matches','teams','stadiums'));
  }

  public function add(){
    return view('matches.add');
  }

  public function match($match){
    $matches = Match::where('id','=',$match)->get();
    $teams = Team::all();
    $stadiums = Stadium::all();
    $riders = Rider::all();
    $gates = Gate::all();
    $results = Result::all();
    $heats = Heat::where('match_id',$match)->get();
    $ridersResults = RidersResults::where('match_id',$match)->get();
    return view('matches.match', compact('matches','teams','stadiums','heats','ridersResults','riders','gates','results'));
  }

  public function show(Request $request){
    $match = new DownloadMatchesDataToDB();
    $match -> downloadMatchData($request->link,$request->game_day);
    return redirect('/matches');
  }
}
