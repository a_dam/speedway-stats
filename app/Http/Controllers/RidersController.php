<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rider;
use App\Team;
use App\Services\RidersParser;
use App\Services\DownloadRidersDataToDB;
use App\Services\IndividualStatsForAllRiders;
use App\Services\IndividualStatsForOneRider;
use App\Services\MatchResultForRider;
use DB;

class RidersController extends Controller
{
	public function __construct(){
		$this->middleware('auth', ['except' => [
            'index', 'rider','stats'
        ]]);
	}

	public function index(){
		$riders = DB::table('riders')
		->join('teams', 'teams.id', '=', 'riders.teams_id')
		->select('riders.*', 'teams.name as team')
		->get();
		return view('riders.index', compact('riders'))	;
	}

	public function stats(){
		$stats = new IndividualStatsForAllRiders();
		$riders = $stats->getRiders();
		if(count($riders)>0){
			$firstPlaces = $stats->getFirstPlaces($riders);
			$secondPlaces = $stats->getSecondPlaces($riders);
			$thirdPlaces = $stats->getThirdPlaces($riders);
			$fourthPlaces = $stats->getFourthPlaces($riders);
			$exclusions = $stats->getExclusions($riders);
			$crashes = $stats->getCrashes($riders);
			$falstarts = $stats->getFalstarts($riders);
			$defects = $stats->getDefects($riders);
			$points = $stats->getPoints($riders,$firstPlaces,$secondPlaces,$thirdPlaces);
			$bonusPoints = $stats->getBonusPoints($riders);
			$pointsWithBonusPoints = $stats->getPointsWithBonusPoints($riders,$points,$bonusPoints);
			$heats = $stats->getHeats($riders);
			$heatAverages = $stats->getHeatAverages($riders,$heats,$pointsWithBonusPoints);
			$matches = $stats->getMatches($riders);
			$matchAverages = $stats->getMatchAverages($riders,$matches,$points);
		}
		return view('riders.stats', compact('riders','firstPlaces' ,'secondPlaces','thirdPlaces','fourthPlaces',
																				'exclusions','defects','crashes','falstarts','points','bonusPoints',
																				'pointsWithBonusPoints','heats','heatAverages','matches','matchAverages'));
	}

	public function  download(){
		return view('riders.download');
	}
	public function add(){
		$teams = Team::all();
		return view('riders.add',compact('teams'));
	}

	public function edit(){
		$riders = DB::table('riders')
		->join('teams', 'teams.id', '=', 'riders.teams_id')
		->select('riders.*', 'teams.name as team')
		->get();;
		return view('riders.edit', compact('riders'));
	}
	public function showUpdateForm(Request $request){
		$rider = Rider::where('id',$request->id)->first();
		$teams = Team::all();
		return view('riders.update',compact('rider','teams'));
	}

	public function save(Request $request){
		$team = Team::where('id',$request->team_id)->first();
		$rider = new Rider();
		$rider->name = $request->input('name');
		$rider->surname = $request->surname;
		$rider->nationality = $request->nationality;
		$rider->date_of_birth = $request->date_of_birth;
		$rider->teams_id = $team->id;
		$rider->save();
		return redirect('/admin/riders/edit');
	}

	public function rider($rider){
		$stats = new IndividualStatsForOneRider();
		$riders = $stats->getRider($rider);
		$firstPlaces = $stats->getFirstPlaces($rider);
		$secondPlaces = $stats->getSecondPlaces($rider);
		$thirdPlaces = $stats->getThirdPlaces($rider);
		$fourthPlaces = $stats->getFourthPlaces($rider);
		$exclusions = $stats->getExclusions($rider);
		$crashes = $stats->getCrashes($rider);
		$falstarts = $stats->getFalstarts($rider);
		$defects = $stats->getDefects($rider);
		$points = $stats->getPoints($rider,$firstPlaces,$secondPlaces,$thirdPlaces);
		$bonusPoints = $stats->getBonusPoints($rider);
		$pointsWithBonusPoints = $stats->getPointsWithBonusPoints($rider,$points,$bonusPoints);
		$heats = $stats->getHeats($rider);
		$heatAverages = $stats->getHeatAverages($rider,$heats,$pointsWithBonusPoints);
		$matches = $stats->getMatches($rider);
		$matchAverages = $stats->getMatchAverages($rider,$matches,$points);
		$matchResultForRider = new MatchResultForRider();
		$matchResults = $matchResultForRider->getResults($rider);
		return view('riders.rider', compact('teams','riders','matches','firstPlaces','secondPlaces','thirdPlaces',
																				'fourthPlaces','exclusions','defects','crashes','falstarts',
																				'bonusPoints','heats','points','pointsWithBonusPoints','heatAverages',
																				'matches','matchAverages','matchResults'));
	}

	public function update(Request $request){
		DB::table('riders')
					->where('id', $request->id)
					->update(['name' => $request->name,'surname' => $request->surname,'nationality' => $request->nationality,'date_of_birth' => $request->date_of_birth,'teams_id' => $request->team_id]);
		return redirect('/admin/riders/edit');
	}

	public function delete(Request $request){
		DB::table('riders')->where('id', $request->id)->delete();
		return redirect('/admin/riders/edit');
	}

	public function show(Request $request){
    $riders = new DownloadRidersDataToDB();
    $riders->downloadRidersData();
		return redirect('/riders');
  }

}
