<?php

use App\Team;
use App\Rider;
use App\Match;
use App\Stadium;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});
Route::get('/register', function () {
    return redirect('login');
});

Route::get('/teams', 'TeamsController@index');
Route::get('/teams/{id}', [
		'uses' => 'TeamsController@team',
		'as' => 'teams.team'
]);
Route::get('/admin/teams/edit', 'TeamsController@edit');
Route::get('/admin/teams/add', 'TeamsController@add');
Route::post('/teams/save', [
		'uses' => 'TeamsController@save',
		'as' => 'teams.save'
]);
Route::post('admin/teams/delete', [
		'uses' => 'TeamsController@delete',
		'as' => 'teams.delete'
]);
Route::post('admin/teams/update/form', [
		'uses' => 'TeamsController@showUpdateForm',
		'as' => 'teams.showUpdateForm'
]);
Route::post('admin/teams/update', [
		'uses' => 'TeamsController@update',
		'as' => 'teams.update'
]);

Route::get('/stadiums', 'StadiumsController@index');
Route::get('/stadiums/{id}', [
		'uses' => 'StadiumsController@stadium',
		'as' => 'stadiums.stadium'
]);
Route::get('/admin/stadiums/edit', 'StadiumsController@edit');
Route::get('/admin/stadiums/add', 'StadiumsController@add');
Route::post('/stadiums/save', [
		'uses' => 'StadiumsController@save',
		'as' => 'stadiums.save'
]);
Route::post('admin/stadiums/delete', [
		'uses' => 'StadiumsController@delete',
		'as' => 'stadiums.delete'
]);
Route::post('admin/stadiums/update/form', [
		'uses' => 'StadiumsController@showUpdateForm',
		'as' => 'stadiums.showUpdateForm'
]);
Route::post('admin/stadiums/update', [
		'uses' => 'StadiumsController@update',
		'as' => 'stadiums.update'
]);

Route::get('/riders', 'RidersController@index');
Route::get('/admin/riders/download', 'RidersController@download');
Route::get('/admin/riders/add', 'RidersController@add');
Route::get('/admin/riders/edit', 'RidersController@edit');
Route::post('/riders/show', [
		'uses' => 'RidersController@show',
		'as' => 'riders.show'
]);
Route::post('/riders/save', [
		'uses' => 'RidersController@save',
		'as' => 'riders.save'
]);
Route::post('admin/riders/delete', [
		'uses' => 'RidersController@delete',
		'as' => 'riders.delete'
]);
Route::post('admin/riders/update/form', [
		'uses' => 'RidersController@showUpdateForm',
		'as' => 'riders.showUpdateForm'
]);
Route::post('admin/riders/update', [
		'uses' => 'RidersController@update',
		'as' => 'riders.update'
]);
Route::get('/riders/{id}', [
		'uses' => 'RidersController@rider',
		'as' => 'riders.rider'
]);
Route::get('/stats', 'RidersController@stats');

Route::get('/matches', 'MatchesController@index');
Route::get('/admin/matches/add', 'MatchesController@add');
Route::get('/matches/show', 'MatchesController@show');
Route::post('/matches/show', [
		'uses' => 'MatchesController@show',
		'as' => 'matches.show'
]);
Route::get('/matches/{id}', [
		'uses' => 'MatchesController@match',
		'as' => 'matches.match'
]);

Route::get('api/v1/teams/{id?}', function($id = null)
{
	if (!$id)
	{
		return Team::all();
	}
	if ($show = Team::find($id))
	{
		return $show;
	}
});

Route::post('api/v1/teams', function(Request $request)
{
	$show = new Team();
	$show->name = Input::get('name');
	$show->city = Input::get('city');
	$show->save();
	return $show;
});

Route::delete('api/v1/teams/{id}', function($id)
{
	if ($show = Team::find($id))
	{
		$show->delete();
		return json_encode(array('message' => 'true'));
	}
});

Route::put('api/v1/teams/{id}', function($id)
{
	if ($show = Team::find($id))
	{
		if (Input::get('name')) {
			$show->name = Input::get('name');
		}
		if (Input::get('year')) {
			$show->city = Input::get('city');
		}
		$show->save();
		return $show;
	}
});
Route::get('api/v1/riders/{id?}', function($id = null)
{
	if (!$id)
	{
		return Rider::all();
	}
	if ($show = Rider::find($id))
	{
		return $show;
	}
});

Route::get('api/v1/matches/{id?}', function($id = null)
{
	if (!$id)
	{
		return Match::all();
	}
	if ($show = Match::find($id))
	{
		return $show;
	}
});

Route::get('api/v1/matches/{id?}', function($id = null)
{
	if (!$id)
	{
		return Match::all();
	}
	if ($show = Match::find($id))
	{
		return $show;
	}
});
Route::get('api/v1/stadiums/{id?}', function($id = null)
{
	if (!$id)
	{
		return Stadium::all();
	}
	if ($show = Stadium::find($id))
	{
		return $show;
	}
});

Route::auth();

Route::post('restLogout', 'LoginController@logout');
Route::post('restLogin', 'LoginController@authenticate');
Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth');

Auth::routes();
