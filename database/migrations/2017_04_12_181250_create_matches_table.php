<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->text('tag');
            $table->integer('stadium_id')->unsigned();
            $table->date('date');
            $table->text('time');
            $table->text('game_day');
            $table->integer('home_team')->unsigned();
            $table->integer('away_team')->unsigned();
            $table->text('result');
            $table->timestamps();
        });

        Schema::table('matches',function (Blueprint $table) {
        	$table->foreign('stadium_id')
        		->references('id')
        		->on('stadia');
        });

        Schema::table('matches',function (Blueprint $table) {
        	$table->foreign('home_team')
        		->references('id')
        		->on('teams');
        });

        Schema::table('matches',function (Blueprint $table) {
        	$table->foreign('away_team')
        		->references('id')
        		->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
