<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidersResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gate_id')->unsigned();
            $table->integer('rider_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->integer('heat_id')->unsigned();
            $table->integer('match_id')->unsigned();
            $table->timestamps();
        });

       	Schema::table('riders_results',function (Blueprint $table) {
       		$table->foreign('gate_id')
       		->references('id')
       		->on('gates');
       	});

       	Schema::table('riders_results',function (Blueprint $table) {
       		$table->foreign('rider_id')
       		->references('id')
       		->on('riders');
       	});

       	Schema::table('riders_results',function (Blueprint $table) {
       		$table->foreign('result_id')
       		->references('id')
       		->on('results');
       	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders_results');
    }
}
