<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $team = new Team();
        $team->name = 'ekantor.pl Falubaz';
        $team->city = "Zielona G\u{00F3}ra" ;
        $team->save();

        $team = new Team();
        $team->name = 'Betard Sparta';
        $team->city = "Wroc\u{0142}aw";
        $team->save();

        $team = new Team();
        $team->name = 'Get Well';
        $team->city = "Toru\u{0144}";
        $team->save();

        $team = new Team();
        $team->name = 'ROW';
        $team->city = 'Rybnik';
        $team->save();

        $team = new Team();
        $team->name = 'FOGO Unia';
        $team->city = 'Leszno';
        $team->save();

        $team = new Team();
        $team->name = 'MrGarden GKM';
        $team->city = "Grudzi\u{0105}dz";
        $team->save();

        $team = new Team();
        $team->name = 'Cash Broker Stal';
        $team->city = "Gorz\u{00F3}w";
        $team->save();

        $team = new Team();
        $team->name = "W\u{0142}\u{00F3}kniarz Vitroszlif Crossfit";
        $team->city = "Cz\u{0119}stochowa";
        $team->save();
    }
}
