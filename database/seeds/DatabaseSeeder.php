<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(StadiasTableSeeder::class);
        $this->call(GatesTableSeeder::class);
        $this->call(ResultsTableSeeder::class);
        $this->call(RidersTableSeeder::class);
        $this->call(MatchesTableSeeder::class);
        $this->call(HeatsTableSeeder::class);
        $this->call(RidersResultsTableSeeder::class);
    }
}
