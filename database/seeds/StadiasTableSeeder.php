<?php

use Illuminate\Database\Seeder;
use App\Stadium;

class StadiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$stadium = new Stadium();
    	$stadium->name = 'SWISS KRONO ARENA';
    	$stadium->city = "Zielona G\u{00F3}ra";
    	$stadium->length_of_track = '337';
    	$stadium->record_of_track = '58,22';
    	$stadium->track_record_holder = "Jaros\u{0142}aw Hampel";
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = 'STADION OLIMPIJSKI';
    	$stadium->city = "Wroc\u{0142}aw";
    	$stadium->length_of_track = '352';
    	$stadium->record_of_track = '65,29';
    	$stadium->track_record_holder = 'Tai Woffinden';
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = 'MOTOARENA';
    	$stadium->city = "Toru\u{0144}";
    	$stadium->length_of_track = '318';
    	$stadium->record_of_track = '58,25';
    	$stadium->track_record_holder = 'Bartosz Zmarzlik';
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = 'STADION MIEJSKI W RYBNIKU';
    	$stadium->city = 'Rybnik';
    	$stadium->length_of_track = '359';
    	$stadium->record_of_track= '63,45';
    	$stadium->track_record_holder = 'Grigorij Laguta';
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = 'STADION IM. ALFREDA SMOCZYKA';
    	$stadium->city = 'Leszno';
    	$stadium->length_of_track = '346';
    	$stadium->record_of_track = '58,53';
    	$stadium->track_record_holder = "Przemys\u{0142}aw Pawlicki";
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = "STADION GKM GRUDZI\u{0104}DZ";
    	$stadium->city = "Grudzi\u{0105}dz";
    	$stadium->length_of_track = '379';
    	$stadium->record_of_track = '64,41';
    	$stadium->track_record_holder = 'Maksim Bognadovs';
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = 'STADION IM. EDWARDA JANCARZA';
    	$stadium->city = "Gorz\u{00F3}w";
    	$stadium->length_of_track = '329';
    	$stadium->record_of_track = '58,00';
    	$stadium->track_record_holder = 'Niels Kristian Iversen';
    	$stadium->save();

    	$stadium = new Stadium();
    	$stadium->name = "SGP ARENA CZ\u{0118}STOCHOWA";
    	$stadium->city = "Cz\u{0119}stochowa";
    	$stadium->length_of_track = '359';
    	$stadium->record_of_track= '62,20';
    	$stadium->track_record_holder = 'Kenneth Bjerre';
    	$stadium->save();
    }
}
