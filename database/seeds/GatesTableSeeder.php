<?php

use Illuminate\Database\Seeder;
use App\Gate;

class GatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gate = new Gate();
        $gate->gate = 'A';
        $gate->save();

        $gate = new Gate();
        $gate->gate = 'B';
        $gate->save();

        $gate = new Gate();
        $gate->gate = 'C';
        $gate->save();

        $gate = new Gate();
        $gate->gate = 'D';
        $gate->save();
    }
}
