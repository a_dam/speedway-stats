<?php

use Illuminate\Database\Seeder;
use App\Result;

class ResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = new Result();
        $result->result = '3';
        $result->save();

        $result = new Result();
        $result->result = '2';
        $result->save();

        $result = new Result();
        $result->result = '1';
        $result->save();

        $result = new Result();
        $result->result = '0';
        $result->save();

        $result = new Result();
        $result->result = 'w';
        $result->save();

        $result = new Result();
        $result->result = 'd';
        $result->save();

        $result = new Result();
        $result->result = 't';
        $result->save();

        $result = new Result();
        $result->result = 'u';
        $result->save();

        $result = new Result();
        $result->result = '2*';
        $result->save();

        $result = new Result();
        $result->result = '1*';
        $result->save();
    }
}
