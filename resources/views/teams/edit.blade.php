@extends('teams.index')

@section('title')
	<?php echo __('messages.manageTeams')?>
@endsection

@section('content')
	<table class="table table-hover">
		<tr>
			<th><?php echo __('messages.name')?></th>
			<th><?php echo __('messages.city')?></th>
			<th colspan="2"><?php echo __('messages.modificate')?></th>
		</tr>
	  @foreach($teams as $team)
		<tr>
			 <td>{{$team->name}}</td>
	   		<td>{{$team->city}}</td>
				<td>
					<form action="{{route('teams.showUpdateForm')}}" method="post">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="id" value="{{$team->id}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
					 	<button class="btn btn-warning btn-xs"><?php echo __('messages.edit')?></button>
					</form>
				</td>
				<td>
					<form action="{{route('teams.delete')}}" method="post">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="id" value="{{$team->id}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
					 	<button class="btn btn-danger btn-xs"><?php echo __('messages.remove')?></button>
					</form>
				</td>
		 </tr>
	 	@endforeach
	</table>
@endsection
