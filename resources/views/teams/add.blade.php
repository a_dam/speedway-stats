@extends('layouts.app')

@section('title')
	<?php echo __('messages.addTeam')?>
@endsection

@section('content')
	<form action="{{route('teams.save')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="form-group">
		 	<input type="text" name="name" class="form-control" placeholder="<?php echo __('messages.typeName')?>">
		</div>
		<div class="form-group">
		 	<input type="text" name="city" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
		</div>
		<div class="form-group">
		 	<button class="btn btn-success"><?php echo __('messages.add')?></button>
		</div>
	</form>
@endsection
