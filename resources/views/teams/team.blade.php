@extends('layouts.app')

@section('title')
	{{$teams[0]->name}} {{$teams[0]->city}}
@endsection

@section('content')
	  @foreach($teams as $team)
	 		<h1 style="text-align:center">{{$team->name}} {{$team->city}}</h1>
	 	@endforeach
		<h3><?php echo __('messages.squad')?></h3>
		<table class="table table-hover">
			<tr>
		 		<th><?php echo __('messages.riderName')?></th>
				<th><?php echo __('messages.riderSurname')?></th>
				<th><?php echo __('messages.riderNationality')?></th>
				<th><?php echo __('messages.riderDateOfBirth')?></th>
		  </tr>
			<tbody data-link="row" class="rowlink">
				@foreach($riders as $rider)
				 <tr>
						<td><a href="{{route('riders.rider',$rider->id)}}">{{$rider->name}}</td>
						<td>{{$rider->surname}}</td>
						<td>{{$rider->nationality}}</td>
						<td>{{$rider->date_of_birth}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
		<h3><?php echo __('messages.matches')?></h3>
		<table class="table table-hover">
			<tr>
				<th><?php echo __('messages.gameDay')?></th>
				<th><?php echo __('messages.date')?></th>
				<th><?php echo __('messages.time')?></th>
				<th><?php echo __('messages.oponnent')?></th>
				<th><?php echo __('messages.result')?></th>
			</tr>
			<tbody data-link="row" class="rowlink">
				@foreach($matches as $match)
				 <tr>
					 	<td>{{$match->game_day}}</td>
						<td>{{$match->date}}</td>
						<td>{{$match->time}}</td>
						<td>{{$match->team}}</td>
						<td>{{$match->result}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
@endsection
