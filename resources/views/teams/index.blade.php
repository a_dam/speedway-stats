@extends('layouts.app')

@section('title')
	<?php echo __('messages.teams')?>
@endsection

@section('content')
	@if (count($teams)>0)
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><?php echo __('messages.name')?></th>
					<th><?php echo __('messages.city')?></th>
				</tr>
			</thead>
			<tbody data-link="row" class="rowlink">
		  	@foreach($teams as $team)
		 	 		<tr>
		   			<td><a href="{{route('teams.team',$team->id)}}">{{$team->name}}</a></td>
		   			<td>{{$team->city}}</td>
		 	 		</tr>
		 		@endforeach
			</tbody>
		</table>
	@else
		<div class="alert alert-danger alert-dismissable" style="text-align:center;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo __('messages.noTeams')?>
		</div>
	@endif
@endsection
