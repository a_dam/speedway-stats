@extends('layouts.app')

@section('title')
	<?php echo __('messages.edit')?>
@endsection

@section('content')
	<form action="{{route('teams.update')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="id" value="{{$team->id}}">
		<div class="form-group">
		 	<input type="text" name="name" value="{{$team->name}}" class="form-control" placeholder="<?php echo __('messages.typeName')?>">
		</div>
		<div class="form-group">
		 	<input type="text" name="city" value="{{$team->city}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
		</div>
		<div class="form-group">
		 	<button class="btn btn-success"><?php echo __('messages.edit')?></button>
		</div>
	</form>
@endsection
