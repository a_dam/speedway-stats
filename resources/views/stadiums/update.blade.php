@extends('layouts.app')

@section('title')
	<?php echo __('messages.edit')?>
@endsection

@section('content')
	<form action="{{route('stadiums.update')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="id" value="{{$stadium->id}}">
		<div class="form-group">
		 	<input type="text" name="name" value="{{$stadium->name}}" class="form-control" placeholder="<?php echo __('messages.typeName')?>">
		</div>
		<div class="form-group">
		 	<input type="text" name="city" value="{{$stadium->city}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
		</div>
		<div class="form-group">
			<input type="text" name="length_of_track" value="{{$stadium->length_of_track}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
		</div>
		<div class="form-group">
			<input type="text" name="record_of_track" value="{{$stadium->record_of_track}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
		</div>
		<div class="form-group">
			<input type="text" name="track_record_holder" value="{{$stadium->track_record_holder}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
		</div>
		<div class="form-group">
		 	<button class="btn btn-success"><?php echo __('messages.edit')?></button>
		</div>
	</form>
@endsection
