@extends('layouts.app')

@section('title')
	{{$stadiums[0]->name}}
@endsection

@section('content')
	  @foreach($stadiums as $stadium)
	 		<h1 style="text-align:center">{{$stadium->name}}</h1>
			<h3 style="text-align:center">{{$stadium->city}}</h3>
			<h4 style="text-align:center"><small><?php echo __('messages.lengthOfTrack')?>: </small> {{$stadium->length_of_track}} m</h4>
			<h4 style="text-align:center"><small><?php echo __('messages.recordOfTrack')?>: </small> {{$stadium->record_of_track}} s</h1>
			<h4 style="text-align:center"><small><?php echo __('messages.holderOfTrackRecord')?>: </small> {{$stadium->track_record_holder}}</h1>

	 	@endforeach
		<h3><?php echo __('messages.stats')?></h3>
		<h4 style="text-align:center"><?php echo __('messages.winsHeatsForGates')?></h1>
		@if($gateA!="" || $gateB!="" || $gateC!="" || $gateD!="")
			<table class="table table-hover">
				<thead>
					<tr>
						<th style="text-align:center"><?php echo __('messages.gate')?> A</th>
						<th style="text-align:center"><?php echo __('messages.gate')?> B</th>
						<th style="text-align:center"><?php echo __('messages.gate')?> C</th>
						<th style="text-align:center"><?php echo __('messages.gate')?> D</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th style="text-align:center">{{$gateA}}</th>
						<th style="text-align:center">{{$gateB}}</th>
						<th style="text-align:center">{{$gateC}}</th>
						<th style="text-align:center">{{$gateD}}</th>
					</tr>
				</tbody>
			</table>
		@else
		<div class="alert alert-danger alert-dismissable" style="text-align:center;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo __('messages.noMatch')?>
		</div>
		@endif
@endsection
