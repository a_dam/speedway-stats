@extends('stadiums.index')

@section('title')
<?php echo __('messages.manageStadiums')?>
@endsection

@section('content')
	<table class="table table-hover">
			<tr>
				<th><?php echo __('messages.name')?></th>
				<th><?php echo __('messages.city')?></th>
				<th><?php echo __('messages.lengthOfTrack')?></th>
				<th><?php echo __('messages.recordOfTrack')?></th>
				<th><?php echo __('messages.holderOfTrackRecord')?></th>
				<th colspan="2"><?php echo __('messages.modificate')?></th>
			</tr>
		  @foreach($stadiums as $stadium)
			<tr>
				 <td>{{$stadium->name}}</td>
		   		<td>{{$stadium->city}}</td>
					<td>{{$stadium->length_of_track}} m</td>
					 <td>{{$stadium->record_of_track}} s</td>
					 <td>{{$stadium->track_record_holder}}</td>
					<td>
						<form action="{{route('stadiums.showUpdateForm')}}" method="post">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="id" value="{{$stadium->id}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
						 	<button class="btn btn-warning btn-xs"><?php echo __('messages.edit')?></button>
						</form>
					</td>
					<td>
						<form action="{{route('stadiums.delete')}}" method="post">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="id" value="{{$stadium->id}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
						 	<button class="btn btn-danger btn-xs"><?php echo __('messages.remove')?></button>
						</form>
					</td>
			 </tr>
		 	@endforeach
		</table>
@endsection
