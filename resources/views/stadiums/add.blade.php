@extends('layouts.app')

@section('title')
	<?php echo __('messages.addStadium')?>
@endsection

@section('content')
<form action="{{route('stadiums.save')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<div class="form-group">
	 	<input type="text" name="name" class="form-control" placeholder="Podaj nazwe">
	</div>
	<div class="form-group">
	 	<input type="text" name="city" class="form-control" placeholder="Podaj miasto">
	</div>
	<div class="form-group">
	 	<input type="text" name="length_of_track" class="form-control" placeholder="Podaj dlugosc toru">
	</div>
	<div class="form-group">
	 	<input type="text" name="record_of_track" class="form-control" placeholder="Podaj rekord toru">
	</div>
	<div class="form-group">
	 	<input type="text" name="track_record_holder" class="form-control" placeholder="Podaj rekordziste toru">
	</div>
	<div class="form-group">
	 	<button class="btn btn-success"><?php echo __('messages.add')?></button>
	</div>
</form>
@endsection
