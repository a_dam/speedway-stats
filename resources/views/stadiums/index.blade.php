@extends('layouts.app')

@section('title')
	<?php echo __('messages.stadiums')?>
@endsection

@section('content')
	@if(count($stadiums)>0)
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><?php echo __('messages.name')?></th>
					<th><?php echo __('messages.city')?></th>
					<th><?php echo __('messages.lengthOfTrack')?></th>
					<th><?php echo __('messages.recordOfTrack')?></th>
					<th><?php echo __('messages.holderOfTrackRecord')?></th>
				</tr>
			</thead>
			<tbody data-link="row" class="rowlink">
			  @foreach($stadiums as $stadium)
			 	 <tr>
			   		<td><a href="{{route('stadiums.stadium',$stadium->id)}}">{{$stadium->name}}</td>
			   		<td>{{$stadium->city}}</td>
			   		<td>{{$stadium->length_of_track}} m</td>
			   		<td>{{$stadium->record_of_track}} s</td>
			   		<td>{{$stadium->track_record_holder}}</td>
			 	 </tr>
			 	@endforeach
			</tbody>
		</table>
	@else
		<div class="alert alert-danger alert-dismissable" style="text-align:center;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo __('messages.noStadiums')?>
		</div>
	@endif
@endsection
