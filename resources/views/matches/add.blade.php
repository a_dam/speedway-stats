@extends('layouts.app')

@section('title')
	<?php echo __('messages.addMatch')?>
@endsection

@section('content')
	<form action="{{route('matches.show')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="input-group">
	    <span class="input-group-addon">http://sportowefakty.wp.pl/zuzel/relacja/</span>
	    <input id="msg" type="text" class="form-control" name="link" placeholder="np. 73084">
	  </div>
		<br>
		<div class="form-group">
		 	<button class="btn btn-success"><?php echo __('messages.download')?></button>
		</div>
	</form>
@endsection
