@extends('layouts.app')

@section('title')
	<?php echo __('messages.matches')?>
@endsection

@section('content')
@if (count($matches)>0)
	<table class="table table-hover">
		<thead>
			<tr>
				<th><?php echo __('messages.gameDay')?></th>
				<th><?php echo __('messages.date')?></th>
				<th><?php echo __('messages.time')?></th>
				<th><?php echo __('messages.place')?></th>
				<th><?php echo __('messages.homeTeam')?></th>
				<th><?php echo __('messages.awayTeam')?></th>
				<th><?php echo __('messages.result')?></th>
			</tr>
		</thead>
		<tbody data-link="row" class="rowlink">
	 	@foreach($matches as $match)
		 <tr>
			 <td><a href="{{route('matches.match',$match->id)}}">{{$match->game_day}}</a></td>
			 <td>{{$match->date}}</td>
			 <td>{{$match->time}}</td>
			 <td>
						@foreach($stadiums as $stadium)
						@if($stadium->id === $match->stadium_id)
						{!! $stadium->name !!}
						@endif
						@endforeach
			 </td>
			 <td>
						@foreach($teams as $team)
						@if($team->id === $match->home_team)
						{!! $team->name !!}
						@endif
						@endforeach
			 </td>
			 <td>
						@foreach($teams as $team)
						@if($team->id === $match->away_team)
						{!! $team->name !!}
						@endif
						@endforeach
			 </td>
			 <td>{{$match->result}}</td>
	 	 </tr>
	 	@endforeach
	</tbody>
	</table>
	@else
	<div class="alert alert-danger alert-dismissable" style="text-align:center;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo __('messages.nullMatch')?>
	</div>
	@endif
@endsection
