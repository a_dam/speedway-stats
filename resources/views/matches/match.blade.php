@extends('layouts.app')

@section('title')
	@foreach($matches as $match)
		@foreach($teams as $team)
			@if($team->id === $match->home_team)
					{{ $team->name }} {{ $team->city }}
			@endif
		@endforeach
		-
		@foreach($teams as $team)
			@if($team->id === $match->away_team)
				{{ $team->name }} {{ $team->city }}
			@endif
		@endforeach
	@endforeach
@endsection

@section('content')
	@foreach($matches as $match)
		<h1 style="text-align:center">
			@foreach($teams as $team)
				@if($team->id === $match->home_team)
						{{ $team->name }} {{ $team->city }}
				@endif
			@endforeach
			-
			@foreach($teams as $team)
				@if($team->id === $match->away_team)
					{{ $team->name }} {{ $team->city }}
				@endif
			@endforeach
		</h1>
		<h1 style="text-align:center">{{$match->result}}</h1>
		<br>
		<h4 style="text-align:center">{{$match->date}}, {{$match->time}}</h4>
		<h4 style="text-align:center">
			@foreach($stadiums as $stadium)
				@if($stadium->id === $match->stadium_id)
					{{ $stadium->name }}
				@endif
			@endforeach
		</h4>
	@endforeach
	<h3><?php echo __('messages.heatsOfMatch')?></h3>
	<table class="table table-striped">
		<tr>
			<th><?php echo __('messages.heat')?></th>
			<th><?php echo __('messages.heatTime')?></th>
			<th><?php echo __('messages.gate')?></th>
			<th><?php echo __('messages.rider')?></th>
			<th><?php echo __('messages.points')?></th>
			<th><?php echo __('messages.result')?></th>
		</tr>
		<tbody>


	@for ($i = 0; $i < count($heats); $i++)
	<tr>
		<tr>
			<td rowspan="4" style="vertical-align: middle;text-align:center;"> {{$heats[$i]->number }}</td>
			<td rowspan="4" style="vertical-align: middle;text-align:center;">{{$heats[$i]->time}}</td>
			<td>A</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($riders); $k++)
							@if($ridersResults[$j]->gate_id==1 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->rider_id==$riders[$k]->id)
								{{ $riders[$k]->name }} {{$riders[$k]->surname}}
							@endif
						@endfor
				@endfor
			</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($results); $k++)
							@if($ridersResults[$j]->gate_id==1 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->result_id==$results[$k]->id)
								{{ $results[$k]->result }}
							@endif
						@endfor
				@endfor
			</td>
			<td rowspan="4" style="vertical-align: middle;text-align:center;">{{$heats[$i]->result}}</td>
		</tr>
		<tr>
			<td>B</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($riders); $k++)
							@if($ridersResults[$j]->gate_id==2 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->rider_id==$riders[$k]->id)
								{{ $riders[$k]->name }} {{$riders[$k]->surname}}
							@endif
						@endfor
				@endfor
			</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($results); $k++)
							@if($ridersResults[$j]->gate_id==2 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->result_id==$results[$k]->id)
								{{ $results[$k]->result }}
							@endif
						@endfor
				@endfor
			</td>
		</tr>
		<tr>
			<td>B</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($riders); $k++)
							@if($ridersResults[$j]->gate_id==3 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->rider_id==$riders[$k]->id)
								{{ $riders[$k]->name }} {{$riders[$k]->surname}}
							@endif
						@endfor
				@endfor
			</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($results); $k++)
							@if($ridersResults[$j]->gate_id==3 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->result_id==$results[$k]->id)
								{{ $results[$k]->result }}
							@endif
						@endfor
				@endfor
			</td>
		</tr>
		<tr>
			<td>D</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($riders); $k++)
							@if($ridersResults[$j]->gate_id==4 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->rider_id==$riders[$k]->id)
								{{ $riders[$k]->name }} {{$riders[$k]->surname}}
							@endif
						@endfor
				@endfor
			</td>
			<td>
				@for ($j = 0; $j < count($ridersResults); $j++)
						@for ($k = 0; $k < count($results); $k++)
							@if($ridersResults[$j]->gate_id==4 && $ridersResults[$j]->heat_id==$heats[$i]->id && $ridersResults[$j]->result_id==$results[$k]->id)
								{{ $results[$k]->result }}
							@endif
						@endfor
				@endfor
			</td>
		</tr>
		</tr>
		@endfor
	</tbody>
	</table>
@endsection
