@extends('layouts.app')
@section('title')
	<?php echo __('messages.downloadRiders')?>
@endsection

@section('content')
	<form action="{{route('riders.show')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="form-group">
		 	<button class="btn btn-success"><?php echo __('messages.download')?></button>
		</div>
	</form>
@endsection
