@extends('riders.index')

@section('title')
	<?php echo __('messages.manageRiders')?>
@endsection

@section('content')
	<table class="table table-hover">
		<tr>
			<th><?php echo __('messages.riderName')?></th>
			<th><?php echo __('messages.riderSurname')?></th>
			<th><?php echo __('messages.riderNationality')?></th>
			<th><?php echo __('messages.riderDateOfBirth')?></th>
			<th><?php echo __('messages.team')?></th>
			<th colspan="2"><?php echo __('messages.modificate')?></th>
		</tr>
	  @foreach($riders as $rider)
		<tr>
			<td>{{$rider->name}}</td>
			<td>{{$rider->surname}}</td>
			<td>{{$rider->nationality}}</td>
			<td>{{$rider->date_of_birth}}</td>
			<td>{{$rider->team}}</td>
				<td>
					<form action="{{route('riders.showUpdateForm')}}" method="post">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="id" value="{{$rider->id}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
					 	<button class="btn btn-warning btn-xs"><?php echo __('messages.edit')?></button>
					</form>
				</td>
				<td>
						<form action="{{route('riders.delete')}}" method="post">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="id" value="{{$rider->id}}" class="form-control" placeholder="<?php echo __('messages.typeCity')?>">
						 	<button class="btn btn-danger btn-xs"><?php echo __('messages.remove')?></button>
						</form>
					</td>
			 </tr>
		 	@endforeach
	</table>
@endsection
