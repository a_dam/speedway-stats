@extends('layouts.app')

@section('title')
<?php echo __('messages.stats')?>
@endsection

@section('content')
	@if (count($riders)>0)
		<div class="alert alert-info alert-dismissable" style="text-align:center;">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <?php echo __('messages.sortHint')?>
		</div>
		<table id="stats" class="table table-striped table-hover">
			<thead>
				<tr>
					<th onclick="sortTable(0)"><?php echo __('messages.riderName')?></th>
					<th onclick="sortTable(1)"><?php echo __('messages.riderSurname')?></th>
					<th onclick="sortTable(2)"><?php echo __('messages.team')?></th>
					<th onclick="sortTable(3)"><?php echo __('messages.matchesShort')?></th>
					<th onclick="sortTable(4)"><?php echo __('messages.heatsShort')?></th>
					<th onclick="sortTable(5)"><?php echo __('messages.firstPlacesShort')?></th>
					<th onclick="sortTable(6)"><?php echo __('messages.secondPlacesShort')?></th>
					<th onclick="sortTable(7)"><?php echo __('messages.thirdPlacesShort')?></th>
					<th onclick="sortTable(8)"><?php echo __('messages.fourthPlacesShort')?></th>
					<th onclick="sortTable(9)"><?php echo __('messages.defectsShort')?></th>
					<th onclick="sortTable(10)"><?php echo __('messages.crashesShort')?></th>
					<th onclick="sortTable(11)"><?php echo __('messages.exclusionsShort')?></th>
					<th onclick="sortTable(12)"><?php echo __('messages.falstartsShort')?></th>
					<th onclick="sortTable(13)"><?php echo __('messages.pointsShort')?></th>
					<th onclick="sortTable(14)"><?php echo __('messages.bonusPointsShort')?></th>
					<th onclick="sortTable(15)"><?php echo __('messages.sumaShort')?></th>
					<th onload="sortTable(16)" onclick="sortTable(16)"><?php echo __('messages.averageShort')?></th>
					<th onclick="sortTable(17)"><?php echo __('messages.matchAverageShort')?></th>
				</tr>
			</thead>
			<tbody data-link="row" class="rowlink">
		  	@for ($i = 0; $i < count($riders); $i++)
					@for ($j = 0; $j < count($firstPlaces); $j++)
						@if ($i == $j && $matches[$j] != 0)
					 	 <tr>
					   		<td><a href="{{route('riders.rider',$riders[$i]->id)}}">{{$riders[$i]->name}}</td>
					   		<td>{{$riders[$i]->surname}}</td>
					   		<td>{{$riders[$i]->team}}</td>
								<td>{{$matches[$j]}}</td>
								<td>{{$heats[$j]}}</td>
								<td>{{$firstPlaces[$j]}}</td>
								<td>{{$secondPlaces[$j]}}</td>
								<td>{{$thirdPlaces[$j]}}</td>
								<td>{{$fourthPlaces[$j]}}</td>
								<td>{{$defects[$j]}}</td>
								<td>{{$crashes[$j]}}</td>
								<td>{{$exclusions[$j]}}</td>
								<td>{{$falstarts[$j]}}</td>
								<td>{{$points[$j]}}</td>
								<td>{{$bonusPoints[$j]}}</td>
								<td>{{$pointsWithBonusPoints[$j]}}</td>
								<td>{{$heatAverages[$j]}}</td>
								<td>{{$matchAverages[$j]}}</td>
						 	 </tr>
							@endif
					  @endfor
		 		@endfor
			</tbody>
		</table>
	@else
		<div class="alert alert-danger alert-dismissable" style="text-align:center;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo __('messages.noRider')?>
		</div>
	@endif
@endsection
