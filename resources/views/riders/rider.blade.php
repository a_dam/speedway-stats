@extends('layouts.app')

@section('title')
	{{$riders[0]->name}} {{$riders[0]->surname}}
@endsection

@section('content')
	  @foreach($riders as $rider)
	 		<h1 style="text-align:center">{{$rider->name}} {{$rider->surname}}</h1>
			<h3 style="text-align:center"><small><?php echo __('messages.team')?>: </small> {{$rider->team}}</h3>
			<h4 style="text-align:center"><small><?php echo __('messages.riderNationality')?>: </small> {{$rider->nationality}}</h4>
			<h4 style="text-align:center"><small><?php echo __('messages.riderDateOfBirth')?>: </small> {{$rider->date_of_birth}}</h1>
	 	@endforeach
		<h3><?php echo __('messages.stats')?></h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th><?php echo __('messages.matches')?></th>
					<th><?php echo __('messages.heats')?></th>
					<th><?php echo __('messages.firstPlaces')?></th>
					<th><?php echo __('messages.secondPlaces')?></th>
					<th><?php echo __('messages.thirdPlaces')?></th>
					<th><?php echo __('messages.fourthPlaces')?></th>
					<th><?php echo __('messages.defects')?></th>
					<th><?php echo __('messages.crashes')?></th>
					<th><?php echo __('messages.exclusions')?></th>
					<th><?php echo __('messages.falstarts')?></th>
					<th><?php echo __('messages.points')?></th>
					<th><?php echo __('messages.bonusPonts')?></th>
					<th><?php echo __('messages.suma')?></th>
					<th><?php echo __('messages.average')?></th>
					<th><?php echo __('messages.matchAverage')?></th>
				</tr>
			</thead>
			<tbody>
				<td>{{$matches}}</td>
				<td>{{$heats}}</td>
				<td>{{$firstPlaces}}</td>
				<td>{{$secondPlaces}}</td>
				<td>{{$thirdPlaces}}</td>
				<td>{{$fourthPlaces}}</td>
				<td>{{$defects}}</td>
				<td>{{$crashes}}</td>
				<td>{{$exclusions}}</td>
				<td>{{$falstarts}}</td>
				<td>{{$points}}</td>
				<td>{{$bonusPoints}}</td>
				<td>{{$pointsWithBonusPoints}}</td>
				<td>{{$heatAverages}}</td>
				<td>{{$matchAverages}}</td>
			</tbody>
		</table>
		<h3><?php echo __('messages.matches')?></h3>
		<table class="table table-hover">
			@if($matchResults!="No results")
			<thead>
				<tr>
					<th><?php echo __('messages.matches')?></th>
				</tr>
			</thead>
		</thead>
		<tbody>
			@foreach($matchResults as $matchResult)
			<tr>
				<td>{{$matchResult}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<div class="alert alert-danger alert-dismissable" style="text-align:center;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo __('messages.noResults')?>
	</div>
	@endif
@endsection
