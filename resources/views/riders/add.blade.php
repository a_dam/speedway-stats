@extends('layouts.app')

@section('title')
	<?php echo __('messages.addRider')?>
@endsection

@section('content')
	<form action="{{route('riders.save')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="form-group">
			<input type="text" name="name" class="form-control" placeholder="<?php echo __('messages.riderName')?>">
		</div>
		<div class="form-group">
			<input type="text" name="surname" class="form-control" placeholder="<?php echo __('messages.riderSurname')?>">
		</div>
		<div class="form-group">
		 	<input type="text" name="nationality" class="form-control" placeholder="<?php echo __('messages.riderNationality')?>">
		</div>
		<div class="form-group">
		 	<input type="text" name="date_of_birth" class="form-control" placeholder="<?php echo __('messages.riderDateOfBirth')?>">
		</div>
		<div class="form-group">
	  	<label for="team_id"><?php echo __('messages.team')?>:</label>
	      <select class="form-control" name="team_id" id="team_id">
					@foreach($teams as $team)
						<option value="{{$team->id}}">{{$team->name}} {{$team->city}}</option>
					@endforeach
	      </select>
	  </div>
		<div class="form-group">
		 	<button class="btn btn-success"><?php echo __('messages.add')?></button>
		</div>
	</form>
@endsection
