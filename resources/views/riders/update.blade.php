@extends('layouts.app')

@section('title')
	<?php echo __('messages.edit')?>
@endsection

@section('content')
<form action="{{route('riders.update')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<input type="hidden" name="id" value="{{$rider->id}}">
	<div class="form-group">
		<input type="text" name="name" value="{{$rider->name}}" class="form-control" placeholder="<?php echo __('messages.riderName')?>">
	</div>
	<div class="form-group">
		<input type="text" name="surname" value="{{$rider->surname}}" class="form-control" placeholder="<?php echo __('messages.riderSurname')?>">
	</div>
	<div class="form-group">
		<input type="text" name="nationality" value="{{$rider->nationality}}" class="form-control" placeholder="<?php echo __('messages.riderNationality')?>">
	</div>
	<div class="form-group">
		<input type="text" name="date_of_birth" value="{{$rider->date_of_birth}}" class="form-control" placeholder="<?php echo __('messages.riderDateOfBirth')?>">
	</div>
	<div class="form-group">
		<label for="team_id"><?php echo __('messages.team')?>:</label>
		<select class="form-control" value="{{$rider->team_id}}"  name="team_id" id="team_id">
			@foreach($teams as $team)
			@if ($rider->teams_id == $team->id)
				<option value="{{$team->id}}" selected>
			@else
				<option  value="{{$team->id}}">
			@endif
			{{$team->name}} {{$team->city}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
	 	<button class="btn btn-success"><?php echo __('messages.edit')?></button>
	</div>
</form>
@endsection
