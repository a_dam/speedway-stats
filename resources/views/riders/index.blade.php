@extends('layouts.app')

@section('title')
<?php echo __('messages.riders')?>
@endsection

@section('content')
	@if(count($riders)>0)
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><?php echo __('messages.riderName')?></th>
					<th><?php echo __('messages.riderSurname')?></th>
					<th><?php echo __('messages.riderNationality')?></th>
					<th><?php echo __('messages.riderDateOfBirth')?></th>
					<th><?php echo __('messages.team')?></th>
				</tr>
			</thead>
			<tbody data-link="row" class="rowlink">
		  	@foreach($riders as $rider)
		 	 		<tr>
			   		<td><a href="{{route('riders.rider',$rider->id)}}">{{$rider->name}}</td>
			   		<td>{{$rider->surname}}</td>
			   		<td>{{$rider->nationality}}</td>
			   		<td>{{$rider->date_of_birth}}</td>
			   		<td>{{$rider->team}}</td>
			 	 	</tr>
		 		@endforeach
			</tbody>
		</table>
	@else
		<div class="alert alert-danger alert-dismissable" style="text-align:center;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo __('messages.noRider')?>
		</div>
	@endif
@endsection
