<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('jasny/bootstrap/dist/css/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <?php echo __('messages.title')?>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="/teams"><?php echo __('messages.teams')?></a></li>
                        <li><a href="/matches"><?php echo __('messages.matches')?></a></li>
                        <li><a href="/stats"><?php echo __('messages.stats')?></a></li>
                        <li><a href="/stadiums"><?php echo __('messages.stadiums')?></a></li>
                        <li><a href="/riders"><?php echo __('messages.riders')?></a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}"><?php echo __('messages.login')?></a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <?php echo __('messages.manage')?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                              <li class="dropdown-header"><?php echo __('messages.teams')?></li>
                              <li><a href="/admin/teams/add"><?php echo __('messages.add')?></a></li>
                              <li><a href="/admin/teams/edit"><?php echo __('messages.modificate')?></a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><?php echo __('messages.matches')?></li>
                              <li><a href="/admin/matches/add"><?php echo __('messages.download')?></a></a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><?php echo __('messages.stadiums')?></li>
                              <li><a href="/admin/stadiums/add"><?php echo __('messages.add')?></a></li>
                              <li><a href="/admin/stadiums/edit"><?php echo __('messages.modificate')?></a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><?php echo __('messages.riders')?></li>
                              <li><a href="/admin/riders/download"><?php echo __('messages.download')?></a></a></li>
                              <li><a href="/admin/riders/add"><?php echo __('messages.add')?></a></a></li>
                              <li><a href="/admin/riders/edit"><?php echo __('messages.modificate')?></a></li>
                            </ul>
                        </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php echo __('messages.loginAs')?>{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <?php echo __('messages.logout')?>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
		<div class="container">
       	 @yield('content')
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('jasny/bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>
    <script>
    // Script from https://www.w3schools.com/howto/howto_js_sort_table.asp
    function sortTable(n) {
      var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
      table = document.getElementById("stats");
      switching = true;
      //Set the sorting direction to ascending:
      dir = "desc";
      /*Make a loop that will continue until
      no switching has been done:*/
      while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
          //start by saying there should be no switching:
          shouldSwitch = false;
          /*Get the two elements you want to compare,
          one from current row and one from the next:*/
          x = rows[i].getElementsByTagName("TD")[n];
          y = rows[i + 1].getElementsByTagName("TD")[n];
          /*check if the two rows should switch place,
          based on the direction, asc or desc:*/
          if (dir == "desc") {
            if (parseFloat(x.innerHTML) < parseFloat(y.innerHTML)) {
              //if so, mark as a switch and break the loop:
              shouldSwitch= true;
              break;
            }
          } else if (dir == "asc") {
            if (parseFloat(x.innerHTML) > parseFloat(y.innerHTML)) {
              //if so, mark as a switch and break the loop:
              shouldSwitch= true;
              break;
            }
          }
        }
        if (shouldSwitch) {
          /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
          //Each time a switch is done, increase this count by 1:
          switchcount ++;
        } else {
          /*If no switching has been done AND the direction is "asc",
          set the direction to "desc" and run the while loop again.*/
          if (switchcount == 0 && dir == "desc") {
            dir = "asc";
            switching = true;
          }
        }
      }
    }
    </script>
    <script>
    window.onload = function() {
        (document.getElementsByTagName( 'th' )[16]).click();
    };
</script>
</body>
</html>
