<?php

return [
    'home' => 'Home',
    'login' => 'Login',
    'title' => 'Speedway Stats',
    'teams' => 'Teams',
    'matches' => 'Matches',
    'stats' => 'Stats',
    'stadiums' => 'Stadiums',
    'riders' => 'Riders',

];
